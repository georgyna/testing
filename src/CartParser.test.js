import CartParser from './CartParser';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('should return array with one error', () => {
		const contents = 'Product name,Price,Quantity\nMollis consequat,3';
		expect(parser.validate(contents)).toHaveLength(1);
	});

	it('should return array with 1 element and call createError method with message about positive number', () => {
		const contents = 'Product name,Price,Quantity\nMollis consequat,-3.00,2';
		const createError = spyOn(parser, 'createError');
		expect(parser.validate(contents)).toHaveLength(1);
		expect(createError).toHaveBeenCalledWith("cell", 1, 1, "Expected cell to be a positive number but received \"-3.00\".");
	});

	it('should return empty error', () => {
		const contents = 'Product name,Price,Quantity';
		expect(parser.validate(contents)).toHaveLength(0);
	});

	it('should return error of errors consist of two elements one of them is about header "Price" header expectation', () => {
		const contents = 'Product name';
		expect(parser.validate(contents)).toHaveLength(2);
		expect(parser.validate(contents)).toContainEqual({
			column: 1,
   			message: 'Expected header to be named \"Price\" but received undefined.',
   			row: 0,
    		type: 'header'
		});
	});

	it('should return object with name Baloon, price 4, quantity 5 and defined id', () => {
		const csvLine = 'Baloon, 4, 5';
		const expected = {
			name: 'Baloon',
			price: 4,
			quantity: 5
		};
		expect(parser.parseLine(csvLine)).toMatchObject(expected);
		expect(parser.parseLine(csvLine).id).not.toBeUndefined();
	});

	it('should return object with name "Some invalid string" and other properties as NaN', () => {
		const csvLine = 'Some invalid string';
		expect(parser.parseLine(csvLine).name).toEqual('Some invalid string');
		expect(parser.parseLine(csvLine).price).toBeNaN();
		expect(parser.parseLine(csvLine).quantity).toBeNaN();
	});

	it('should calculate the total price of items', () => {
		const items = [{ name: 'item1', price: 2, quantity: 10 }, { name: 'item2', price: 1, quantity: 3 }];
		expect(parser.calcTotal(items)).toEqual(23);
	});

	it('should return NaN if some property which is used for calculations is absent', () => {
		const items = [{ name: 'item1' }, { name: 'item2', price: '1', quantity: 3 }];
		expect(parser.calcTotal(items)).toBeNaN();
	});

	it('should creates an error with type, row, column and message passed to method', () => {
		expect(parser.createError('errorType', 3, 4, 'Not found')).toEqual({
			type: 'errorType',
			row: 3,
			column: 4,
			message: 'Not found'
		});
	});

	it('should returnes object with undefined message and column if message and column are not passed to createError method', () => {
		expect(parser.createError('errorType', 'Not found')).toEqual({
			type: 'errorType',
			row: 'Not found',
			column: undefined,
			message: undefined
		});
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('should parse cart.csv file and return object with 5 items and total 348.32', () => {
		const filePath = "samples/cart.csv";
		expect(parser.parse(filePath).items).toHaveLength(5);
		expect(parser.parse(filePath).total).toEqual(348.32);
	});
});